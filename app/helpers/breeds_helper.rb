module BreedsHelper
  def get_breed_list
   begin
    breeds_list = JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list/all").body)["message"]
    breeds_list.keys
    rescue Object => e
      []
    end
  end
end
