$(document).ready(function () {
  $('#breed').on('change',function(){ 
    breed = $('#breed').val();
    $.ajax({
    url: "https://dog.ceo/api/breed/"+breed+"/images/random",
    method: "get",
    dataType: "json",
    success: function (data) {
     console.log(data);
     $('.image').empty();
     $('.image').append('<img src="'+data.message+'">');
    }
  });
    
  });
});